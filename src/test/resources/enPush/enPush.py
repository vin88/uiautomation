import os
import binascii
import requests
import logging
import configparser
import sys

try:
    
    #Default Settings
    logFilePath = sys.argv[1]
    print("log file is %s", logFilePath)
    logging.basicConfig(filename=logFilePath, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y-%I:%M:%S')


    #config file read
    configFilePath = sys.argv[2]
    print("config file is %s", configFilePath)
    config = configparser.ConfigParser()
    config.read(configFilePath)

    #http headers
    headers = {}

    if config.get('Packet_Format', 'Packet_Type') == "CL":
        headers['ConextCL-IP'] = config.get('CL_Http_Headers', 'ConextCL-IP')
        headers['ConextCL-MAC'] = config.get('CL_Http_Headers', 'ConextCL-MAC')
        headers['GATEWAYTYPE'] = config.get('CL_Http_Headers', 'GATEWAYTYPE')
        headers['ConextCL-Name'] = config.get('CL_Http_Headers', 'ConextCL-Name')
    elif config.get('Packet_Format', 'Packet_Type') == "COMBOX":
        headers['ComBox-IP'] = config.get('ComBox_Http_Headers', 'ComBox-IP')
        headers['ComBox-MAC'] = config.get('ComBox_Http_Headers', 'ComBox-MAC')
        headers['GATEWAYTYPE'] = config.get('ComBox_Http_Headers', 'GATEWAYTYPE')
        headers['ComBox-Name'] = config.get('ComBox_Http_Headers', 'ComBox-Name')
        

    #filename = config.get('Setting', 'File') -- we will use filename from command line
    filename = sys.argv[3]
    print("json file is %s", filename)
    URL = config.get('Setting', 'Endpoint')
    encoded_string = ""

    #AES encryption
    if config.get('Packet_Format', 'Packet_Type') != "SMARTBOX":
        if os.system('aescrypt -e -p "n97ttDlzEi6MKM6SMWGOAgO8IrbLnHfhv3sitE674dDQUc6T5GZ6gYRNIPf74RBu" %s' % filename) != 0:
            logging.info("Error While encrypting - %s" % (filename))

        filename = filename.replace('"','')

        #base64 encoding
        with open("%s.aes"%(filename), "rb") as aes_file:
            encoded_string = binascii.b2a_base64(aes_file.read())
            #print("base64 data is --%s--" % (encoded_string))
            aes_file.close()

        with open("%s.aes"%(filename), "wb") as aes_file:
            aes_file.write(encoded_string)
            aes_file.close()
    else:
        with open(filename, "rb") as file:
            encoded_string = file.read()
            
    #data push
    if config.get('Setting', 'Proxy'):
        """provide respective proxy IP(like 'http://103.27.171.32:80') under C:\config\config.ini 'Proxy' key when you are in VPN/office network, else leave it blank"""
        http_proxy = config.get('Setting', 'Proxy')
        proxydict = {'http': http_proxy}
        r = requests.post(URL, data=encoded_string, headers=headers, proxies=proxydict)
        #print("headers:%s" % headers)
        #r = requests.post(URL, data=encoded_string, headers=headers)
        logging.info("File:%s - Httpcode:%s - HttpMessage:%s" %(filename, r.status_code, r.text))
    else:
        """ sending without proxy settings. keep Proxy key value as empty in config.ini"""
        print("headers:%s" % headers)
        r = requests.post(URL, data=encoded_string, headers=headers)
        print("File:%s - Httpcode:%s - HttpMessage:%s" %(filename, r.status_code, r.text))
        logging.info("File:%s - Httpcode:%s - HttpMessage:%s" %(filename, r.status_code, r.text))

except Exception as e:
    logging.info("File:%s - Error: %s" %(filename, str(e)))
