package UIPackage;
import org.openqa.selenium.By;
import org.testng.Assert;

/**
 * Created by SESA89230 on 6/14/2016.
 */
public class forgotPassword extends UIHelper {

    public String answer1 = "acharya";
    public String answer2 = "nethravathi";
    public String forgotPasswordSubmit = "register";
    public String validateAnswersSubmit = "btnSubmit";
    public String NewPassword = "Vin@15";

    public void forgotPasswordClick(){
        driver.getTitle();
        driver.findElement(By.xpath
                ("//*[@id=\'LoginForm\']/div/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td[3]/a[1]")).click();
        driver.getTitle();
    }

    public void validateForgotPasswordPage(){
        driver.getTitle();
        String str = driver.findElement(By.xpath("//*[@id='ForgotPasswordForm']/div[1]")).getText();
        driver.getTitle();
        Assert.assertEquals(str, "Forgot Password", "Mismatch");
    }

    public String forgotPasswordGetCaptcha(){
        driver.getTitle();
        String str = "";
        int num = 1;
        while(num <= 5){
            str+=(driver.findElementByXPath
                    ("//*[@id=\'ForgotPasswordForm\']/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/div/label/span["+num+"]").getText());
            num++;
        }
        driver.getTitle();
        return str;
    }

    public void forgotPasswordSetCaptcha(String capchta){
        driver.findElementById("ArithmeticValue").sendKeys(capchta);
    }

    public void forgotPasswordSetAnswer1(String str){
        driver.findElementById("Answer1").sendKeys(str);
    }

    public void forgotPasswordSetAnswer2(String str){
        driver.findElementById("Answer2").sendKeys(str);
    }

    public void newPassword(String password){
        driver.findElementById("progressBarID").sendKeys(password);
    }

    public void confirmPassword(String password){
        driver.findElementById("ConfirmPassword").sendKeys(password);
    }

    public void submitResetPassword(){
        //driver.getTitle();
        driver.findElement
                (By.xpath("//*[@id='ResetpasswordForm']/table/tbody/tr/td[2]/table[1]/tbody/tr[5]/td[3]/div/input[1]")).submit();
        //driver.getTitle();
    }

    public void submitByID(String ID){
        driver.findElementById(ID).submit();
    }
}
