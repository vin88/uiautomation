package UIPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by sesa89230 on 12/8/2016.
 */

public class addNewSite extends loginPage {

    public String AddSiteButton_tag = "AddSiteButton";
    public String GatewayType_tag = "siteType";
    public String GatewayTypeCombox = "Conext ComBox";
    public String GatewayTypeCL = "Conext CL";
    public String SiteName_tag = "s_name";
    public String SiteSizeBattery_tag = "SiteSizeBattery";
    public String BatterySize = "10";
    public String SiteSizePV_tag = "SiteSizePvKw";
    public String PVSiteSize = "25";
    public String SiteAddress_tag = "siteAdrr_input";
    public String SiteAddress = "Gauribidanur";
    public String SiteSearchButton_tag = "MapSearchButton";
    public String SiteSubmitButton_tag = "register";


    public void clickAddSite(){

        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className(AddSiteButton_tag)));
        ee.click();
    }

    public void selectComboxGateway(String GatewayType){
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id(GatewayType_tag)));
        Select se = new Select(ee);
        se.selectByVisibleText(GatewayType);
    }

    public void siteName(String SiteName){
        driver.findElement(By.id(SiteName_tag)).sendKeys(SiteName);
    }

    public void commissionedDate(){

        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tblvalidate\"]/tbody/tr[7]/td[2]/img[contains(@class, 'ui-datepicker-trigger dateButton')]")));
        ee.click();
        driver.getTitle();
        List<WebElement> ef = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id=\"ui-datepicker-div\"]/table[contains(@class, 'ui-datepicker-calendar')]//td")));

        for(WebElement ele:ef)
        {

            String date=ele.getText();

            if(date.equalsIgnoreCase("1"))
            {
                ele.click();
                break;
            }

        }
    }

    public void siteSizeBattery(){
        driver.findElement(By.id(SiteSizeBattery_tag)).sendKeys(BatterySize);
    }

    public void siteSizePV(){
        driver.findElement(By.id(SiteSizePV_tag)).sendKeys(PVSiteSize);
    }

    public void siteAddressSearch(){
        driver.findElement(By.id(SiteAddress_tag)).sendKeys(SiteAddress);
        driver.findElement(By.className(SiteSearchButton_tag)).click();
    }

    public void submitSiteCreattion(){
        driver.findElement(By.id(SiteSubmitButton_tag)).submit();
    }

    public String validateSiteCreation(){
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("popup_message")));
        return ee.getText();
    }

}
