package UIPackage;
/**
 * Created by SESA89230 on 6/6/2016.
 */

/*import org.apache.commons.configuration2.*;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;*/
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.apache.commons.io.IOUtils;
import java.io.File;


public class UIHelper {

    public FirefoxDriver driver = new FirefoxDriver();
    public String ComboxSiteName = "sanityTest-Combox";
    public String CLSiteName = "sanityTest-CL";
    public String seconduser = "vinodspeakto@gmail.com";
    public String username = "conextinsight@yahoo.com";
    public String password = "Password123"; //yahoo login password: SolarMonitoring2017
    public String URL = "http://preproductioncbconextinsight.cloudapp.net/";
    public String ver = "3.43";
    public String ComboxMAC = "CB:00:00:00:00:CB";
    public String ComboxSerial = "B12345678";
    public String ComboxName = "combox1";
    public String CLMAC = "CC:11:11:11:11:CC";
    public String CLSerial = "ZX1234567891";
    public String CLName = "cl1";

/*    public UIHelper(){
        username = "vinodspeakto@gmail.com";
        password = "VIN@15";
        URL = "http://preproductioncbconextinsight.cloudapp.net/";
    }*/

    //Open link on Browser
    public void openPage(){
        driver.navigate().to(URL);
        driver.manage().window().maximize();
    }

    //Validate Checkbox checked or not
    public boolean checkboxValidation(){
        return driver.findElementById("RememberMe").isSelected();
    }

    //Checking RememberMe Checkbox
    public void rememberMeCheck(){
        if(!checkboxValidation()){
            driver.findElementById("RememberMe").click();
        }
    }

    //Unchecking RememberMe Checkbox
    public void rememberMeUnCheck(){
        if(checkboxValidation()){
            driver.findElementById("RememberMe").click();
        }
    }

    //Login to Link
    public void toLogin(){
        //driver.findElementById("ddlpic_msa_0").click();
        driver.findElementById("Email").sendKeys(username);
        driver.findElementById("Password").sendKeys(password);
        driver.findElementById("register").click();
    }

    //Login Page Browser text
    public String loginBrowserText(){
        return driver.findElementByClassName("login_text").getText();
    }

    public void logout(){
        /*UIHelper helper = new UIHelper();
        helper.driver.getTitle();
        helper.driver.findElement(By.xpath("/*//*[@id=\'bs-SE-navbar-collapse-1\']/div[1]/ul/li[1]/ul/li[2]")).click();
        helper.driver.getTitle();*/
    }

    public void forgotPasswordClick(){}

    public void forgotPasswordEmail(String username){
        driver.findElementById("Email").sendKeys(username);
    }

    public void validateForgotPasswordPage(){}

    public String forgotPasswordGetCaptcha(){ return null;}

    public void forgotPasswordSetCaptcha(String capchta){}

    public void forgotPasswordSetAnswer1(String str){}

    public void forgotPasswordSetAnswer2(String str){}

    public void submitByID(String ID){}

    public void newPassword(String password){}

    public void confirmPassword(String password){}

    public void submitResetPassword(){}

}

/*
//To use chrome driver, replace UIPackage/UIHelper.java:21 codeline with below code snippet
String Path = System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver_win32\\chromedriver.exe";
System.setProperty("webdriver.chrome.driver", Path);
WebDriver chromeDriver = new ChromeDriver();
chromeDriver.navigate().to("https://preproductioncbconextinsight.cloudapp.net");
*/
