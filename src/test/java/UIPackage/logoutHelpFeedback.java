package UIPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by SESA89230 on 1/2/2017.
 */
public class logoutHelpFeedback extends loginPage {

    public void help(){
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/Content/Docs/ConextInsightRevisedHelpDoc.pdf']")));
        ee.click();
    }

    public void logout(){
        Actions actions = new Actions(driver);
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("logOutNameStyle")));
        actions.moveToElement(ee);

        WebElement eee = driver.findElement(By.cssSelector("a[href*='/en/Home/Logout']"));
        actions.moveToElement(eee);
        actions.click().build().perform();
    }

    public void feedback(){

        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/en/UserProfile/FeedBack']")));
        ee.click();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='/Content/simpleDialog/Close.png']")));
        driver.findElement(By.id("Feedback2Answer")).sendKeys("good");
        driver.findElement(By.id("Feedback3Answer")).sendKeys("good");
        driver.findElement(By.id("Feedback3Answer")).submit();
    }
}
