package UIPackage;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by sesa89230 on 12/12/2016.
 */
public class testSearchSite {

    searchSite helper;

    @BeforeMethod
    public void setUpMethod(){
        helper = new searchSite();
        helper.openPage();
        helper.toLogin();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void searchSiteByName(String SiteName) throws InterruptedException {
        helper.clickSiteDetails(SiteName);
        String str = helper.validateSiteDetail();
        Assert.assertEquals(str, helper.ComboxSiteName);
    }
}
