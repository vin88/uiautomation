package UIPackage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.xml.bind.annotation.XmlMimeType;

/**
 * Created by SESA89230 on 1/11/2017.
 */
public class testManageCLSite {

    manageCLSite helper;

    @BeforeMethod
    public void setUpMethod(){
        helper = new manageCLSite();
        helper.openPage();
        helper.toLogin();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void addCLDevice() throws InterruptedException {
        helper.addCL(helper.CLSiteName);
    }

    @Test
    public void deleteCLSite() throws InterruptedException {
        helper.deleteSite(helper.CLSiteName);
    }

    @Test
    public void deleteCLDevice() throws InterruptedException {
        helper.deleteCL(helper.CLSiteName);
    }

    @Test
    public void eventsCLActiveAlarm() throws InterruptedException {
        helper.eventsActiveAlarm(helper.CLSiteName);
    }

    @Test
    public void eventsCLActiveWarning() throws InterruptedException {
        helper.eventsActiveWarning(helper.CLSiteName);
    }

    @Test
    public void trendsCLInverters() throws InterruptedException {
        helper.trendsInverters(helper.CLSiteName);
    }

    @Test
    public void addCLUser() throws InterruptedException {
        helper.addUser(helper.CLSiteName);
    }

    @Test
    public void deleteCLUser() throws InterruptedException {
        helper.deleteUser(helper.CLSiteName);
    }
}
