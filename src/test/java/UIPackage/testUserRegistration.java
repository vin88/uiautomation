package UIPackage;
import  org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import org.testng.Assert;


/**
 * Created by SESA89230 on 12/2/2016.
 */
public class testUserRegistration {

    userRegistration helper = null;

    @BeforeMethod
    public void setUpMethod(){
        helper = new userRegistration();
        helper.openPage();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void registerUser(){
        helper.registerPage();
        helper.prefix();
        helper.firstName();
        helper.lastName();
        helper.userType();
        helper.email();
        helper.password();
        helper.confirmPassword();
        helper.secretQuestion1();
        helper.secretAnswer1();
        helper.secretQuestion2();
        helper.secretAnswer2();
        helper.phoneCountryCode();
        helper.phoneNumber();
        helper.City();
        helper.Country();
        String str = helper.registrationGetCaptcha();
        helper.registrationSetCaptcha(str);
        helper.UserRegistrationPrivacyPolicy();
        helper.submitRegistration();
    }
}
