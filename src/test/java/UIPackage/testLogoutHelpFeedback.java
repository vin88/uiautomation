package UIPackage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by SESA89230 on 1/2/2017.
 */
public class testLogoutHelpFeedback {

    logoutHelpFeedback helper;

    @BeforeMethod
    public void setUpMethod(){
        helper = new logoutHelpFeedback();
        helper.openPage();
        helper.toLogin();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void helpLink(){
        helper.help();
    }

    @Test
    public void logoutLink(){
        helper.logout();
    }

    @Test
    public void feedbackLink(){
        helper.feedback();
    }

}
