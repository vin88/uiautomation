package UIPackage;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * Created by SESA89230 on 12/19/2016.
 */
public class dataPush {

    String cd = System.getProperty("user.dir") + "\\src\\test\\resources\\enPush";

    public void push(String jsonfile, String configFile) throws IOException, InterruptedException {

        String logfile = cd + "\\log\\log.log "; //keep space after string
        String configfile = cd + "\\config\\"+configFile+" "; //keep space after string

        Process process = Runtime.getRuntime().exec("python " + cd + "\\enPush.py "+logfile+configfile+jsonfile);
        System.out.println("python " + cd + "\\enPush.py "+logfile+configfile+jsonfile);

        if(process.waitFor() == 0){
            System.out.println(jsonfile+" data push happened Successfully!");
        }
        else{
            System.out.println(jsonfile+" data push failed. Please check "+logfile+"for more details");
        }
    }

    @Test
    public void updateAndPush() throws IOException, InterruptedException {

        copyFiles();
        List<String> comboxfiles = new ArrayList<String>();
        String comboxDir = cd + "\\jsonFiles\\comboxTemplateJsonFiles";
        File[] comboxFiles = new File(comboxDir).listFiles();
        List<String> clfiles = new ArrayList<String>();
        String clDir = cd + "\\jsonFiles\\conextCLTemplateJsonFiles";
        File[] clFiles = new File(clDir).listFiles();

        try{
            for(File file : comboxFiles){
                if(file.isFile()){
                    comboxfiles.add(file.getName());
                }
            }
            for(File file : clFiles){
                if(file.isFile()){
                    clfiles.add(file.getName());
                }
            }

            //JSONArray a = (JSONArray) parser.parse(new FileReader("c:\\exer4-courses.json"));

            for(String str : comboxfiles){
                push(comboxDir + "\\" + str, "COMBOX.ini");
            }

            for(String str : clfiles){
                push(clDir + "\\" + str, "CL.ini");
            }
        }
        finally {
            FileUtils.deleteDirectory(new File(comboxDir));
            FileUtils.deleteDirectory(new File(clDir));
        }
    }

    public void copyFiles(){

        String src = cd + "\\templateJsonFiles";
        String dest = cd + "\\jsonFiles";

        File srcDir = new File(src);
        File destDir = new File(dest);

        try{
            FileUtils.copyDirectory(srcDir, destDir);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
