package UIPackage;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

/**
 * Created by SESA89230 on 1/11/2017.
 */
public class manageCLSite extends searchSite {

    public void addCL(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divEvents']")));
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        /*WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement mysite = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        waaait.until(ExpectedConditions.visibilityOf(mysite));
        waaait.until(ExpectedConditions.elementToBeClickable(mysite));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", mysite);*/



        //inverters section
        WebDriverWait waaaait = new WebDriverWait(driver, 20);
        WebElement inverters = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#Inverters']")));
        waaaait.until(ExpectedConditions.visibilityOf(inverters));
        waaaait.until(ExpectedConditions.elementToBeClickable(inverters));
        JavascriptExecutor executor1111 = (JavascriptExecutor) driver;
        executor1111.executeScript("arguments[0].click();", inverters);

        wait.until( new Predicate<WebDriver>() {
                        public boolean apply(WebDriver driver) {
                            return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                        }
                    }
        );
        Integer int1 = 0;
        while(int1 != 10000000){
            int1 += 1;
        }
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);



        //WebElement name1  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"upperPanel\"]/tbody/tr[1]/td/div[1]")));
        //Assert.assertEquals(name1.getText(), "Plant One Line");

        //WebElement name2  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("divChildDisplay")));
        //Assert.assertEquals(name2.getText(), "one-line");

        //WebElement name3  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("lblInverter")));
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //Assert.assertEquals(name3.getText(), "Inverters");

        //WebElement name4  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"divMySiteInverters\"]/div[1]/table/thead/tr/th[2]/div/a")));
        //Assert.assertEquals(name4.getText(), "FGA Number");

        Integer int2 = 0;
        while(int2 != 10000000){
            int2 += 1;
        }
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //click installed ac capacity section
        /*WebDriverWait wait2 = new WebDriverWait(driver, 20);
        WebElement capacity = wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("ui-accordion-accordion4-header-1")));
        wait2.until(ExpectedConditions.visibilityOf(capacity));
        wait2.until(ExpectedConditions.elementToBeClickable(capacity));
        JavascriptExecutor executor11111 = (JavascriptExecutor) driver;
        executor11111.executeScript("arguments[0].click();", capacity);

//       driver.findElement(By.id("popup_ok")).click();

        WebDriverWait wait4 = new WebDriverWait(driver, 20);
        WebElement capvalue = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.name("InstalledCapacity")));
        wait4.until(ExpectedConditions.visibilityOf(capvalue));
        capvalue.clear();
        capvalue.sendKeys("10");
        //driver.findElement(By.name("InstalledCapacity")).sendKeys("10");
        driver.findElement(By.xpath("/*//*[@id=\"divSBInstalledCapacity\"]/table/tbody/tr[2]/td[2]/input")).click();
        WebDriverWait wait3 = new WebDriverWait(driver, 20);
        WebElement clickok = wait3.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        wait3.until(ExpectedConditions.visibilityOf(clickok));
        clickok.click();*/


        //add inverter
        WebDriverWait wit = new WebDriverWait(driver, 30);
        WebElement addinv = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/en/SB_Inverter/SB_CreateInverter']")));
        Thread.sleep(3000);
        wit.until(ExpectedConditions.visibilityOf(addinv));
        wit.until(ExpectedConditions.elementToBeClickable(addinv));

        while(!addinv.isDisplayed()){
            continue;
        }

        while(!addinv.isEnabled()){
            continue;
        }
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", addinv);

        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);


        //for(String winHandle : driver.getWindowHandles()){
        //    driver.switchTo().window(winHandle);
        //}

        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement form = builder.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"sd_container\"]/div[1]")));
        //By.xpath("//img[@src='/Content/simpleDialog/Close.png']")
        Thread.sleep(3000);
        builder.until(ExpectedConditions.visibilityOf(form));
        builder.until(ExpectedConditions.elementToBeClickable(form));
//        builder.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(form));
        //invName.sendKeys(CLName);

        //for(String winHandle : driver.getWindowHandles()){
        //    driver.switchTo().window(winHandle);
        //}

        driver.findElement(By.id("InverterName")).sendKeys(CLName);
        driver.findElement(By.id("MacId")).sendKeys(CLMAC);
        driver.findElement(By.id("SerialNumber")).sendKeys(CLSerial);
        driver.findElement(By.id("register")).click();

        Thread.sleep(2000);
        WebElement popadded = driver.findElement(By.id("popup_message"));
        Assert.assertEquals(popadded.getText(), "Inverter Successfully added.");


        //By.xpath("//*[@id=\"UsersData\"]/div/div/div[2]/table/tbody/tr[2]/td[5]/div[2]/a/div[contains(@class, 'header ui-state-default ui-corner-top')]")

    }

    public void deleteSite(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divPlantOne']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement mysite = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        waaait.until(ExpectedConditions.visibilityOf(mysite));
        waaait.until(ExpectedConditions.elementToBeClickable(mysite));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", mysite);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait waaaait = new WebDriverWait(driver, 20);
        WebElement site = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.id("site")));
        waaaait.until(ExpectedConditions.visibilityOf(site));
        waaaait.until(ExpectedConditions.elementToBeClickable(site));
        JavascriptExecutor executor1111 = (JavascriptExecutor) driver;
        executor1111.executeScript("arguments[0].click();", site);

        //Thread.sleep(50);
        //driver.getTitle();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

       // WebElement srch = driver.findElement(By.className("MapSearchButton"));
        //Actions ac = new Actions(driver).click();
        //ac.moveToElement(srch).build().perform();


        WebDriverWait pvwait = new WebDriverWait(driver, 50);
        WebElement sitename = pvwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"FormInfo\"]/div[1]/div[2]/div[1]/table/tbody/tr[5]/td[2]")));
        //*[@id="FormInfo"]/div[2][contains(@onclick, 'DeleteSite()')]
        pvwait.until(ExpectedConditions.visibilityOf(sitename));
        //pvwait.until(ExpectedConditions.elementToBeClickable(del));
        Assert.assertEquals(sitename.getText(), "sanityTest-CL");

        WebElement gatewaytype  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"FormInfo\"]/div[1]/div[2]/div[1]/table/tbody/tr[4]/td[2]")));
        Assert.assertEquals(gatewaytype.getText(), "Conext CL");

        WebElement datecommission  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"FormInfo\"]/div[1]/div[2]/div[1]/table/tbody/tr[8]/td[2]")));
        Assert.assertEquals(datecommission.getText(), "01-Jan-2017");

        WebElement pvsize  = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"FormInfo\"]/div[1]/div[2]/div[1]/table/tbody/tr[9]/td[2]")));
        Assert.assertEquals(pvsize.getText(), "25");


        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        WebDriverWait waaaaait = new WebDriverWait(driver, 20);
        WebElement del = waaaaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@onclick, 'DeleteSite()')]")));
        waaaaait.until(ExpectedConditions.visibilityOf(del));
        waaaaait.until(ExpectedConditions.elementToBeClickable(del));
        while(!del.isEnabled()){
            continue;
        }

        JavascriptExecutor executor11111 = (JavascriptExecutor) driver;
        executor11111.executeScript("arguments[0].click();", del);

        WebDriverWait waaaaaait = new WebDriverWait(driver, 50);
        WebElement popdel = waaaaaait.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        popdel.click();
    }

    public void deleteCL(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divPlantOne']")));
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //inverters section
        WebDriverWait waaaait = new WebDriverWait(driver, 20);
        WebElement inverters = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#Inverters']")));
        waaaait.until(ExpectedConditions.visibilityOf(inverters));
        waaaait.until(ExpectedConditions.elementToBeClickable(inverters));
        JavascriptExecutor executor1111 = (JavascriptExecutor) driver;
        executor1111.executeScript("arguments[0].click();", inverters);

        wait.until( new Predicate<WebDriver>() {
                        public boolean apply(WebDriver driver) {
                            return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                        }
                    }
        );
        Integer int1 = 0;
        while(int1 != 10000000){
            int1 += 1;
        }
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Integer int2 = 0;
        while(int2 != 10000000){
            int2 += 1;
        }


        //add inverter
        WebDriverWait delinvwait = new WebDriverWait(driver, 30);
        WebElement delinv = delinvwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"divMySiteInverters\"]/div[1]/table/tbody/tr/td[6]/div[2]/a")));
        Thread.sleep(3000);
        delinvwait.until(ExpectedConditions.visibilityOf(delinv));
        delinvwait.until(ExpectedConditions.elementToBeClickable(delinv));

        while(!delinv.isDisplayed()){
            continue;
        }

        while(!delinv.isEnabled()){
            continue;
        }
        JavascriptExecutor executor2 = (JavascriptExecutor) driver;
        executor2.executeScript("arguments[0].click();", delinv);

        Thread.sleep(1000);
        WebDriverWait delwait = new WebDriverWait(driver, 30);
        WebElement delokay = delwait.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        delwait.until(ExpectedConditions.visibilityOf(delokay));
        delwait.until(ExpectedConditions.elementToBeClickable(delokay));
        while(!delokay.isEnabled()){
            continue;
        }
        JavascriptExecutor executor3  = (JavascriptExecutor) driver;
        executor3.executeScript("arguments[0].click();", delokay);

        Thread.sleep(1000);
        WebElement popdeleted = driver.findElement(By.id("popup_message"));
        Assert.assertEquals(popdeleted.getText(), "Inverter Deleted Successfully");
    }

    public void eventsActiveAlarm(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement events = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divEvents']")));
        wait.until(ExpectedConditions.visibilityOf(events));
        wait.until(ExpectedConditions.elementToBeClickable(events));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", events);

        Thread.sleep(2000);
        WebElement activealarm = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#SB_ActiveErrorSummaryDiv']")));
        while(!activealarm.isEnabled()){
            continue;
        }
        activealarm.click();
        Thread.sleep(1000);

        WebElement alarmcode = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"SB_ActiveAlarmDiv\"]/div[2]/table/tbody/tr[2]/td[6]")));
        Assert.assertEquals(alarmcode.getText(), "2450");
    }

    public void eventsActiveWarning(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement events = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divEvents']")));
        wait.until(ExpectedConditions.visibilityOf(events));
        wait.until(ExpectedConditions.elementToBeClickable(events));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", events);

        Thread.sleep(2000);
        WebElement activealarm = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#SB_ActiveWarningSummaryDiv']")));
        while(!activealarm.isEnabled()){
            continue;
        }
        activealarm.click();
        Thread.sleep(1000);

        WebElement alarmcode = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"SB_ActiveWarningDiv\"]/div[2]/table/tbody/tr[2]/td[5]")));
        Assert.assertEquals(alarmcode.getText(), "4656");
    }

    public void trendsInverters(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement trends = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divMySite1']")));
        wait.until(ExpectedConditions.visibilityOf(trends));
        wait.until(ExpectedConditions.elementToBeClickable(trends));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", trends);

        Thread.sleep(2000);
        WebElement activealarm = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("TrendsInverters")));
        while(!activealarm.isEnabled()){
            continue;
        }
        activealarm.click();
        Thread.sleep(1000);

        WebElement alarmcode = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("inverterName")));
        Assert.assertEquals(alarmcode.getText(), "100");
    }

    public void addUser(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divPlantOne']")));
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //inverters section
        WebDriverWait waaaait = new WebDriverWait(driver, 20);
        WebElement inverters = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.id("userdiv")));
        waaaait.until(ExpectedConditions.visibilityOf(inverters));
        waaaait.until(ExpectedConditions.elementToBeClickable(inverters));
        JavascriptExecutor executor1111 = (JavascriptExecutor) driver;
        executor1111.executeScript("arguments[0].click();", inverters);

        Thread.sleep(2000);
        WebElement adduser = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/en/SiteUsers/AddSiteUser']")));
        while(!adduser.isEnabled()){
            continue;
        }
        adduser.click();
        Thread.sleep(2000);
        WebElement addbutton = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.name("EmailID")));
        addbutton.sendKeys(seconduser);

        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("newUserRole")));
        Select se = new Select(ee);
        se.selectByVisibleText("Site User");
        driver.findElement(By.id(" addbtn")).click();
        Thread.sleep(1000);
        WebElement popup = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"popup_message\"]")));
        String str = popup.getText();
        org.testng.Assert.assertEquals(str.contains("User is added to this site successfully."),true);
    }

    public void deleteUser(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#divPlantOne']")));
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"LeftDivStyle\"]/ul/li[7][contains(@class, 'header ui-state-default ui-corner-top')]")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //inverters section
        WebDriverWait waaaait = new WebDriverWait(driver, 20);
        WebElement inverters = waaaait.until(ExpectedConditions.presenceOfElementLocated(By.id("userdiv")));
        waaaait.until(ExpectedConditions.visibilityOf(inverters));
        waaaait.until(ExpectedConditions.elementToBeClickable(inverters));
        JavascriptExecutor executor1111 = (JavascriptExecutor) driver;
        executor1111.executeScript("arguments[0].click();", inverters);

        Thread.sleep(2000);





        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement deleteuser = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"divSite\"]/div/div/div[2]/table/tbody/tr[2]/td[5]/div[2]/a")));
        waaait.until(ExpectedConditions.visibilityOf(deleteuser));
        waaait.until(ExpectedConditions.elementToBeClickable(deleteuser));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", deleteuser);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        Thread.sleep(1000);


        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        builder.until(ExpectedConditions.visibilityOf(close));
        close.click();
    }

}
