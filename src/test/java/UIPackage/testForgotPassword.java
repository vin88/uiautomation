package UIPackage;
import org.testng.annotations.*;
import org.testng.Assert;

/**
 * Created by SESA89230 on 6/10/2016.
 */

public class testForgotPassword {

    forgotPassword helper = null;

    @BeforeMethod
    public void setUpMethod(){
        helper = new forgotPassword();
        helper.openPage();
        helper.forgotPasswordClick();
        helper.driver.getTitle();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void validateForgotPasswordPage(){
        helper.validateForgotPasswordPage();
    }

    @Test
    public void forgotPasswordEmail(){
        helper.forgotPasswordEmail(helper.username);
    }

    @Test
    public void submitForgotPasswordPage(){
        helper.forgotPasswordEmail(helper.username);
        String str = helper.forgotPasswordGetCaptcha();
        helper.forgotPasswordSetCaptcha(str);
        helper.submitByID(helper.forgotPasswordSubmit);
    }

    @Test
    public void submitValidateAnswersPage(){
        submitForgotPasswordPage();
        helper.forgotPasswordSetAnswer1(helper.answer1);
        helper.forgotPasswordSetAnswer2(helper.answer2);
        helper.submitByID(helper.validateAnswersSubmit);
    }

    @Test
    public void resetPasswordPage(){
        submitValidateAnswersPage();
        helper.newPassword(helper.NewPassword);
        helper.confirmPassword(helper.NewPassword);
        helper.submitResetPassword();
    }
}


