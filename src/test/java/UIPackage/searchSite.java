package UIPackage;

/**
 * Created by sesa89230 on 12/12/2016.
 */

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class searchSite extends addNewSite {

    public String SearchButton_tag = "btnSearch";


    public void searchSiteWithName(String SiteName){
        WebElement ee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tags\"][contains(@class, 'form-control ui-autocomplete-input')]")));
        ee.sendKeys(SiteName);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        /*Actions actions = new Actions(driver);
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"tags\"][contains(@class, 'form-control ui-autocomplete-input')]")));
        actions.moveToElement(ee);


        WebElement eeee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.className("ui-corner-all")));
        actions.moveToElement(eeee);
        actions.click().build().perform();*/
    }

    public void clickSearchButton(){
        //WebElement ee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id(SearchButton_tag)));
        //Actions ac = new Actions(driver);
        //ac.moveToElement(ee).click().perform();
        //driver.findElement(By.id(SearchButton_tag)).click();
    }

    public void clickSiteDetails(String SiteName) throws InterruptedException {
        searchSiteWithName(SiteName);
        //clickSearchButton();
        /*Thread.sleep(5);
        WebElement ee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions ac = new Actions(driver).doubleClick(ee);
        ac.build().perform();*/
    }

    public String validateSiteDetail(){
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("breadcrumb-lastchild")));
        return ee.getText();
    }
}
