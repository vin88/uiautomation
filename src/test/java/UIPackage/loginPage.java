package UIPackage;

import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

/**
 * Created by SESA89230 on 6/14/2016.
 */
public class loginPage extends UIHelper {



    //Checking RememberMe Checkbox
    public void rememberMeCheck(){
        if(!checkboxValidation()){
            driver.findElementById("RememberMe").click();
        }
    }

    //Un checking RememberMe Checkbox
    public void rememberMeUnCheck(){
        if(checkboxValidation()){
            driver.findElementById("RememberMe").click();
        }
    }

    //Login to Link
    public void toLogin(){
        //driver.findElementById("ddlpic_msa_0").click();
        driver.findElementById("Email").sendKeys(username);
        driver.findElementById("Password").sendKeys(password);
        driver.findElementById("register").click();
    }

    //Login Page Browser text
    public String loginBrowserText(){
        return driver.findElementByClassName("login_text").getText();
    }

}
