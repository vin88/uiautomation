package UIPackage;
import org.testng.annotations.*;
import org.testng.Assert;

/**
 * Created by SESA89230 on 6/8/2016.
 */
public class testLoginPage {

//    UIHelper helper = null;
    loginPage helper = null;

    @BeforeMethod
    public void setUpMethod(){
        //helper = new UIHelper();
        helper = new loginPage();
        helper.openPage();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void checkTitleBeforeLogin() {
        Assert.assertEquals(helper.driver.getTitle(), "SE Solar-Conext Insight", "Invalid Title");
    }

    @Test
    public void rememberMeCheck(){
        helper.rememberMeCheck();
    }

    @Test
    public void remeberMeUncheck(){
        helper.rememberMeUnCheck();
    }

    @Test
    public void loginBrowserText(){
        Assert.assertEquals(helper.loginBrowserText(),
                "Recommended browser: Google Chrome 31 ++, Firefox 34 ++, IE 10 ++ onwards.", "Mismatch");
    }

    @Test
    public void login(){
        helper.toLogin();
    }
}
