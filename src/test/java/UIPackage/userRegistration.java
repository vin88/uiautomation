package UIPackage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by SESA89230 on 12/2/2016.
 */
public class userRegistration extends UIHelper {



    public String Prefix = "Mr";
    public String FirstName = "vinod";
    public String LastName = "kumar";
    public String TypeOfUser = "Solar Installer";
    public String Email = "vubagubegu@stromox.com";
    public String Password = "Password123";
    public String PhoneCountryCode = "080";
    public String PhoneNumber = "123456789";
    public String City = "Bengaluru";
    public String Country = "India";
    public String SecretQuestion1 = "What is your pet name?";
    public String SecretAnswer1 = "dog";
    public String SecretQuestion2 = "What is your first school name?";
    public String SecretAnswer2 = "acharya";

    //html tag names
    public String Prefix_tag = "PrefixId";
    public String FirstName_tag = "FirstName";
    public String LastName_tag = "LastName";
    public String UserType_tag = "TypeId";
    public String Email_tag = "Email";
    public String Password_tag = "progressBarID";
    public String ConfirmPassword_tag = "ConfirmPassword";
    public String SecretQuestion1_tag = "Question1";
    public String SecretAnswer1_tag = "Answer1";
    public String SecretQuestion2_tag = "Question2";
    public String SecretAnswer2_tag = "Answer2";
    public String PhoneCountryCode_tag = "Countrycode_Phoneno";
    public String PhoneNumber_tag = "PhoneNo";
    public String City_tag = "AddressObject_City";
    public String Country_tag = "AddressObject_CountryId";
    public String PrivayPolicy_tag = "PrivacyPolicy";
    public String NewUserRegistration_tag = "/en-US/User/NewUserRegistration";

    public void registerPage(){
        driver.findElement(By.cssSelector("a[href*='/en-US/User/NewUserRegistration']")).click();
    }

    public String registrationGetCaptcha(){
        String str = "";
        int num = 1;
        while(num <= 5){
            str+=(driver.findElementByXPath
                    ("//*[@id=\"RegisterForm\"]/div[3]/div[2]/table/tbody/tr[10]/td[1]/div/label/span["+num+"]").getText());
            num++;
        }
        return str;
    }

    public void registrationSetCaptcha(String capchta){
        driver.findElementById("ArithmeticValue").sendKeys(capchta);
    }

    public void prefix(){
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id(Prefix_tag)));
        Select se = new Select(ee);
        se.selectByVisibleText("Mr");
    }

    public void firstName(){
        driver.findElementById(FirstName_tag).sendKeys(FirstName);
    }

    public void lastName(){
        driver.findElementById(LastName_tag).sendKeys(LastName);
    }

    public void userType(){
        Select dropdown = new Select(driver.findElement(By.id(UserType_tag)));
        dropdown.selectByVisibleText(TypeOfUser);
    }

    public void email(){
        driver.findElementById(Email_tag).sendKeys(Email);
    }

    public void password(){
        driver.findElementById(Password_tag).sendKeys(Password);
    }

    public void confirmPassword(){
        driver.findElementById(ConfirmPassword_tag).sendKeys(Password);
    }

    public void secretQuestion1(){
        Select dropdown1 = new Select(driver.findElement(By.name(SecretQuestion1_tag)));
        //dropdown1.selectByVisibleText(SecretQuestion1);
        dropdown1.selectByIndex(1);
    }

    public void secretAnswer1(){
        driver.findElementById(SecretAnswer1_tag).sendKeys(SecretAnswer1);
    }

    public void secretQuestion2(){
        Select dropdown2 = new Select(driver.findElement(By.id(SecretQuestion2_tag)));
        dropdown2.selectByVisibleText(SecretQuestion2);
    }

    public void secretAnswer2(){
        driver.findElementById(SecretAnswer2_tag).sendKeys(SecretAnswer2);
    }

    public void phoneCountryCode(){
        driver.findElementById(PhoneCountryCode_tag).sendKeys(PhoneCountryCode);
    }

    public void phoneNumber(){
        driver.findElementById(PhoneNumber_tag).sendKeys(PhoneNumber);
    }

    public void City(){
        driver.findElementById(City_tag).sendKeys(City);
    }

    public void Country(){
        Select dropdown = new Select(driver.findElement(By.id(Country_tag)));
        dropdown.selectByVisibleText(Country);
    }

    public void UserRegistrationPrivacyPolicy(){
        driver.findElementById(PrivayPolicy_tag).click();
    }

    public void submitRegistration(){
        driver.findElement(By.id("register")).submit();
    }
}
