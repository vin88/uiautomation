package UIPackage;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by sesa89230 on 12/12/2016.
 */
public class testManageComboxSite {

    manageComboxSite helper;

    @BeforeMethod
    public void setUpMethod(){
        helper = new manageComboxSite();
        helper.openPage();
        helper.toLogin();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void deleteSite() throws InterruptedException {
        helper.deleteSite(helper.ComboxSiteName);
    }

    @Test
    public void addSiteUser() throws InterruptedException {
        helper.adduser(helper.ComboxSiteName);
    }

    @Test
    public void addComboxDevice() throws InterruptedException {
        helper.addCombox(helper.ComboxSiteName);
    }

    @Test
    public void solarEnergyTab() throws InterruptedException {
        helper.solarEnergy(helper.ComboxSiteName);
    }

    @Test
    public void gridEnergyTab() throws InterruptedException {
        helper.gridEnergy(helper.ComboxSiteName);
    }

    @Test
    public void batteryEnergyTab() throws InterruptedException {
        helper.batteryEnergy(helper.ComboxSiteName);
    }

    @Test
    public void loadEnergyTab() throws InterruptedException {
        helper.loadEnergy(helper.ComboxSiteName);
    }

    @Test
    public void generatorEnergyTab() throws InterruptedException {
        helper.generatorEnergy(helper.ComboxSiteName);
    }

    @Test
    public void devicesTab() throws InterruptedException {
        helper.devices(helper.ComboxSiteName);
    }

    @Test
    public void eventsErrorTab() throws InterruptedException {
        helper.eventsError(helper.ComboxSiteName);
    }

    @Test
    public void eventsWarningTab() throws InterruptedException {
        helper.eventsWarning(helper.ComboxSiteName);
    }

    @Test
    public void deleteSiteDevice() throws InterruptedException {
        helper.deleteDevice(helper.ComboxSiteName);
    }

    @Test
    public void deleteSiteUser() throws InterruptedException {
        helper.deleteuser(helper.ComboxSiteName);
    }
}
