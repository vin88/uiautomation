package UIPackage;

import junit.framework.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by sesa89230 on 12/8/2016.
 */
public class testAddNewSite {

    addNewSite helper;

    @BeforeMethod
    public void setUpMethod(){
        helper = new addNewSite();
        helper.openPage();
        helper.toLogin();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void addComboxSite(){
        helper.clickAddSite();
        helper.selectComboxGateway(helper.GatewayTypeCombox);
        helper.siteName(helper.ComboxSiteName);
        helper.commissionedDate();
        helper.siteSizeBattery();
        helper.siteAddressSearch();
        helper.submitSiteCreattion();
        String str = helper.validateSiteCreation();
        org.testng.Assert.assertEquals(str.contains("Site has been added"),true);
    }

    @Test
    public void addCLSite(){
        helper.clickAddSite();
        helper.selectComboxGateway(helper.GatewayTypeCL);
        helper.siteName(helper.CLSiteName);
        helper.commissionedDate();
        helper.siteSizePV();
        helper.siteAddressSearch();
        helper.submitSiteCreattion();
        String str = helper.validateSiteCreation();
        org.testng.Assert.assertEquals(str.contains("Site has been added"),true);
    }
}
