package UIPackage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by sesa89230 on 12/12/2016.
 */
public class manageComboxSite extends searchSite {

    public void deleteSite(String SiteName) throws InterruptedException {
        clickSiteDetails(SiteName);
        driver.getTitle();
//        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
//        WebElement ee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tags\"][contains(@class, 'form-control ui-autocomplete-input')]")));
//        ee.sendKeys(SiteName);
        //WebElement ee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\'tags\']")));
        //ee.sendKeys(Keys.TAB);

        //WebElement delbtn = driver.findElement(By.id("btnSearch"));
        //Thread.sleep(10);
        //WebElement delbtn = (new WebDriverWait(driver, 20)).until(ExpectedConditions.elementToBeClickable(By.id("btnSearch")));
        //WebElement srch = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tags\"][contains(@class, 'form-control ui-autocomplete-input')]")));
        //WebDriverWait wait = new WebDriverWait(driver, 20);
        //WebElement srch = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("MapSearchButton")));
        //wait.until(ExpectedConditions.visibilityOf(srch));
        //wait.until(ExpectedConditions.elementToBeClickable(srch));

        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        //JavascriptExecutor executor = (JavascriptExecutor) driver;
        //executor.executeScript("arguments[0].click();", delbtn);

        //Actions builder = new Actions(driver);
        //builder.moveToElement(srch).build().perform();
        //builder.moveToElement(delbtn).build().perform();
        //delbtn.click();



        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //WebDriverWait wait = new WebDriverWait(driver,10);
        //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ui-id-4")));
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divMySite']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        //Actions actions = new Actions(driver);
        //Thread.sleep(10);
        //actions.moveToElement(eeee).click().perform();
        //Thread.sleep(10);
        //gotoMysite();
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);

        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        //Thread.sleep(10);
        //WebElement del = driver.findElement(By.id("btnDelete"));
        //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement del = waaait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnDelete")));
        waaait.until(ExpectedConditions.visibilityOf(del));
        waaait.until(ExpectedConditions.elementToBeClickable(del));
        Actions ac1 = new Actions(driver).click();
        ac1.moveToElement(del).perform();
        //JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        //executor11.executeScript("arguments[0].click();", del);
        //WebElement dele = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("btnDelete")));
        //Actions bc = new Actions(driver).click(dele);
        //bc.build().perform();
        WebElement delcnfrm = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        delcnfrm.click();
    }

    public void adduser(String SiteName) throws InterruptedException {
        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divMySite']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#UsersData']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);

        //driver.findElement(By.cssSelector("a[href*='#UsersData']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement adduser = waaait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/en/SiteUsers/AddSiteUser']")));
        waaait.until(ExpectedConditions.visibilityOf(adduser));
        waaait.until(ExpectedConditions.elementToBeClickable(adduser));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", adduser);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }


        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[4]/div[3]")));
        builder.until(ExpectedConditions.visibilityOf(close));
        //email.sendKeys("vinodspeakto@gmail.com");

        driver.findElement(By.name("EmailID")).sendKeys(seconduser);
        //WebElement email = cssWait("EmailID");
        //email.sendKeys("vinodspeakto@gmail.com");
        WebElement ee = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("newUserRole")));
        Select se = new Select(ee);
        se.selectByVisibleText("Site User");
        driver.findElement(By.id(" addbtn")).click();
        WebElement popup = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"popup_message\"]")));
        String str = popup.getText();
        org.testng.Assert.assertEquals(str.contains("User is added to this site successfully."),true);

    }

    public void addCombox(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divMySite']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#ComboxDiv']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);

        //driver.findElement(By.cssSelector("a[href*='#UsersData']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement combox = waaait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='/en/CB_SiteManagement/CreateCombox']")));
        waaait.until(ExpectedConditions.visibilityOf(combox));
        waaait.until(ExpectedConditions.elementToBeClickable(combox));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", combox);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[4]/div[3]")));
        builder.until(ExpectedConditions.visibilityOf(close));

        driver.findElement(By.id("ComboxName")).sendKeys(ComboxName);
        driver.findElement(By.id("MacId")).sendKeys(ComboxMAC);
        driver.findElement(By.id("SerialNumber")).sendKeys(ComboxSerial);
        driver.findElement(By.id("register")).click();
    }

    public void solarEnergy(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#SolarEnergyDiv']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement daily = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tblTotalEnergyView\"]/tbody/tr/td[1]/div/span[2]")));
        waaait.until(ExpectedConditions.visibilityOf(daily));
        Assert.assertEquals(daily.getText(), "2");
    }

    public void gridEnergy(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#GridEnergyDiv']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement submit = waaait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnSubmit")));
        waaait.until(ExpectedConditions.visibilityOf(submit));
        waaait.until(ExpectedConditions.elementToBeClickable(submit));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", submit);
    }

    public void batteryEnergy(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#BatteryEnergyDiv']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement submit = waaait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnSubmit")));
        waaait.until(ExpectedConditions.visibilityOf(submit));
        waaait.until(ExpectedConditions.elementToBeClickable(submit));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", submit);
    }

    public void loadEnergy(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#LoadEnergyDiv']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement submit = waaait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnSubmit")));
        waaait.until(ExpectedConditions.visibilityOf(submit));
        waaait.until(ExpectedConditions.elementToBeClickable(submit));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", submit);
    }

    public void generatorEnergy(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#GeneratorEnergyDiv']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);


        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement submit = waaait.until(ExpectedConditions.presenceOfElementLocated(By.id("btnSubmit")));
        waaait.until(ExpectedConditions.visibilityOf(submit));
        waaait.until(ExpectedConditions.elementToBeClickable(submit));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", submit);
    }

    public void devices(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divDivices']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"divDivices\"]/div/table/tbody/tr[4]/td[2]/a")));
        org.testng.Assert.assertEquals(user.getText(), ComboxName+"()");
    }

    public void eventsError(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divEvents']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ActiveErrorDiv\"]/div/label")));
        org.testng.Assert.assertEquals(user.getText(), "Active Errors");
    }

    public void eventsWarning(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divEvents']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#WarningSummaryDiv']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);

        //driver.findElement(By.cssSelector("a[href*='#UsersData']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        driver.getTitle();
        WebElement combox = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"noActiveRowTr\"]/td")));
        System.out.println(combox.getText());
        org.testng.Assert.assertEquals(combox.getText(), "No Active Warnings are present for this site.");
    }

    public void deleteDevice(String SiteName) throws InterruptedException {

        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divMySite']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#ComboxDiv']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);

        //driver.findElement(By.cssSelector("a[href*='#UsersData']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement combox = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ComboxDiv\"]/div/div/table/tbody/tr/td[5]/div[2]/a/div[contains(@class, 'ui-icon ui-icon-trash')]")));
        //By.xpath("//*[@id=\"ui-datepicker-div\"]/table[contains(@class, 'ui-datepicker-calendar')]//td")
        waaait.until(ExpectedConditions.visibilityOf(combox));
        waaait.until(ExpectedConditions.elementToBeClickable(combox));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", combox);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        builder.until(ExpectedConditions.visibilityOf(close));

        close.click();
    }

    public void deleteuser(String SiteName) throws InterruptedException {
        clickSiteDetails(SiteName);
        driver.getTitle();
        WebElement srch = driver.findElement(By.className("MapSearchButton"));
        Actions ac = new Actions(driver).click();
        ac.moveToElement(srch).build().perform();
        WebElement eee = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*//*[@id=\"SearchSiteMapDiv\"]/div/div/div[1]/div[4]/div[3]/div[contains(@class, 'gmnoprint')]")));
        Actions acc = new Actions(driver).doubleClick(eee);
        acc.build().perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement eeee = driver.findElement(By.cssSelector("a[href*='#divMySite']"));
        WebDriverWait waait = new WebDriverWait(driver, 10);
        waait.until(ExpectedConditions.visibilityOf(eeee));
        waait.until(ExpectedConditions.elementToBeClickable(eeee));
        JavascriptExecutor executor1 = (JavascriptExecutor) driver;
        executor1.executeScript("arguments[0].click();", eeee);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement user = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[href*='#UsersData']")));
        wait.until(ExpectedConditions.visibilityOf(user));
        wait.until(ExpectedConditions.elementToBeClickable(user));
        JavascriptExecutor executor11 = (JavascriptExecutor) driver;
        executor11.executeScript("arguments[0].click();", user);

        //driver.findElement(By.cssSelector("a[href*='#UsersData']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebDriverWait waaait = new WebDriverWait(driver, 20);
        WebElement deleteuser = waaait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"UsersData\"]/div/div/div[2]/table/tbody/tr[2]/td[5]/div[2]/a/div[contains(@class, 'ui-icon ui-icon-trash')]")));
        waaait.until(ExpectedConditions.visibilityOf(deleteuser));
        waaait.until(ExpectedConditions.elementToBeClickable(deleteuser));
        JavascriptExecutor executor111 = (JavascriptExecutor) driver;
        executor111.executeScript("arguments[0].click();", deleteuser);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }


        WebDriverWait builder = new WebDriverWait(driver, 20);
        WebElement close = builder.until(ExpectedConditions.presenceOfElementLocated(By.id("popup_ok")));
        builder.until(ExpectedConditions.visibilityOf(close));
        close.click();

    }

}
