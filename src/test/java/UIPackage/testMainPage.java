package UIPackage;
import org.testng.annotations.*;
import org.testng.Assert;

/**
 * Created by SESA89230 on 6/9/2016.
 */
public class testMainPage {
    UIHelper helper = null;

    @BeforeMethod
    public void setUpMethod(){
        helper = new UIHelper();
        helper.openPage();
    }

    @AfterMethod
    public void tearDownMethod(){
        helper.driver.close();
        helper.driver.quit();
    }

    @Test
    public void versionCheck(){
        helper.toLogin();
        Assert.assertEquals(helper.driver.findElementById("spVersion").getText(), helper.ver, "Version value wrong");
    }

    @Test
    public void logout(){
        helper.toLogin();
        helper.logout();
    }

    @Test
    public void checkTitleAfterLogin(){
        helper.toLogin();
        Assert.assertEquals(helper.driver.getTitle(), "SE Solar-Conext Insight", "Invalid Title");
    }

    
}
